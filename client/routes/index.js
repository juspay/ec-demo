import React from 'react'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import rootReducer from '../reducers/index'

import Layout from '../containers/Layout'
import HomeView from '../containers/HomeView'
import PaymentOptionsView from '../containers/PaymentOptionsView'
import APIPaymentMethodsView from '../containers/APIPaymentMethodsView'
import PayV3PaymentMethodsView from '../containers/PayV3PaymentMethodsView'
import CardPaymentView from '../containers/CardPaymentView'
import NetBankingPaymentView from '../containers/NetBankingPaymentView'
import WalletPaymentView from '../containers/WalletPaymentView'
import PayV3CardPaymentView from '../containers/PayV3CardPaymentView'
import PayV3NetBankingPaymentView from '../containers/PayV3NetBankingPaymentView'
import PayV3WalletPaymentView from '../containers/PayV3WalletPaymentView'
import PaymentLinksView from '../containers/PaymentLinksView'
import IFramePaymentView from '../containers/IFramePaymentView'
import PaymentReceiptView from '../containers/PaymentReceiptView'

const store = createStore(
	rootReducer,
	applyMiddleware(thunk)
)

var routes = (
	<Provider store={store}>
		<Router history={browserHistory}>
			<Route path='/' component={Layout}>
				<IndexRoute component={HomeView} />
				<Route path='/orders/:orderId/paymentoptions' component={PaymentOptionsView}/>
				<Route path='/orders/:orderId/api/paymentmethods' component={APIPaymentMethodsView} />
				<Route path='/orders/:orderId/pay-v3/paymentmethods' component={PayV3PaymentMethodsView} />
				<Route path='/orders/:orderId/api/pay/card' component={CardPaymentView} />
				<Route path='/orders/:orderId/api/pay/netbanking' component={NetBankingPaymentView} />
				<Route path='/orders/:orderId/api/pay/wallet' component={WalletPaymentView} />
				<Route path='/orders/:orderId/pay-v3/pay/card' component={PayV3CardPaymentView} />
				<Route path='/orders/:orderId/pay-v3/pay/netbanking' component={PayV3NetBankingPaymentView} />
				<Route path='/orders/:orderId/pay-v3/pay/wallet' component={PayV3WalletPaymentView} />
				<Route path='/orders/:orderId/paymentlinks' component={PaymentLinksView} />
				<Route path='/orders/:orderId/ipay' component={IFramePaymentView} />
				<Route path='/orders/:orderId/receipt' component={PaymentReceiptView} />
				<Route path='*' component={HomeView} />
			</Route>
		</Router>
	</Provider>
);

module.exports = routes;