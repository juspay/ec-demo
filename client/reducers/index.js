import { combineReducers } from 'redux';
import orderDetails from './OrderDetails'
import cardDetails from './CardDetails'
import nbDetails from './NBDetails'
import walletDetails from './WalletDetails'

const rootReducer = combineReducers({
	orderDetails,
	cardDetails,
	nbDetails,
	walletDetails
});

export default rootReducer;