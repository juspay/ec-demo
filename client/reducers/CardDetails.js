import { handleActions } from 'redux-actions'
import {
	UPDATE_CARD_NUMBER,
	UPDATE_CARD_EXPIRY_MONTH,
	UPDATE_CARD_EXPIRY_YEAR,
	UPDATE_CARD_SECURITY_CODE,
	UPDATE_CARD_TOKEN,
	UPDATE_CARD_BRAND,
	UPDATE_STORED_CARDS,
	UPDATE_CARD_VALIDATION,
	UPDATE_SAVE_TO_LOCKER,
	SET_ACTIVE_TAB
} from '../constants/ActionTypes'

const initialState = {
	storedCards : [],
	cardToken : '',
	cardNumber : '',
	cardExpiryMonth : '',
	cardExpiryYear : '',
	cardSecurityCode : '',
	cardBrand : '',
	saveToLocker : false,
	activeTab : 'saved_cards',
	storedCardsRetrieved : false,
	cardValidation : {
		cardExpiryMonthEmpty : false,
		cardExpiryYearEmpty : false,
		cardSecurityCodeEmpty : false,
		cardNumberEmpty : false,
		cardExpiryMonthInvalid : false,
		cardExpiryYearInvalid : false,
		cardSecurityCodeInvalid : false,
		cardNumberInvalid : false
	}
}

export default handleActions({
	[SET_ACTIVE_TAB] : (state, {payload}) => {
		return Object.assign({}, state, {activeTab : payload})
	},
	[UPDATE_CARD_NUMBER] : (state, {payload}) => {
		return Object.assign({}, state, {cardNumber : payload})
	},
	[UPDATE_CARD_TOKEN] : (state, {payload}) => {
		return Object.assign({}, state, {cardToken : payload})
	},
	[UPDATE_CARD_EXPIRY_MONTH] : (state, {payload}) => {
		return Object.assign({}, state, {cardExpiryMonth : payload})
	},
	[UPDATE_CARD_EXPIRY_YEAR] : (state, {payload}) => {
		return Object.assign({}, state, {cardExpiryYear : payload})
	},
	[UPDATE_CARD_SECURITY_CODE] : (state, {payload}) => {
		return Object.assign({}, state, {cardSecurityCode : payload})
	},
	[UPDATE_CARD_BRAND] : (state, {payload}) => {
		return Object.assign({}, state, {cardBrand : payload})
	},
	[UPDATE_STORED_CARDS] : (state, {payload}) => {
		return Object.assign({}, state, {storedCards : payload, storedCardsRetrieved : true})
	},
	[UPDATE_SAVE_TO_LOCKER] : (state, {payload}) => {
		return Object.assign({}, state, {saveToLocker : payload})
	},
	[UPDATE_CARD_VALIDATION] : (state, {payload}) => {
		return Object.assign({}, state, {cardValidation : payload})
	}
}, initialState)