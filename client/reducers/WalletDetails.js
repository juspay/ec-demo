import { handleActions } from 'redux-actions'
import {
	SET_WALLET_OPTIONS,
	SET_SELECTED_WALLET,
	SET_SELECTED_WALLET_LABEL,
	UPDATE_WALLET_VALIDATION
} from '../constants/ActionTypes'

const initialState = {
	walletOptions : [],
	selectedWallet : '',
	selectedWalletLabel : 'Choose a wallet',
	validation : {
		selectedWalletEmpty : false
	}
}

export default handleActions({
	[SET_WALLET_OPTIONS] : (state, {payload}) => {
		return Object.assign({}, state, {walletOptions : payload})
	},
	[SET_SELECTED_WALLET] : (state, {payload}) => {
		return Object.assign({}, state, {selectedWallet : payload})
	},
	[SET_SELECTED_WALLET_LABEL] : (state, {payload}) => {
		return Object.assign({}, state, {selectedWalletLabel : payload})
	},
	[UPDATE_WALLET_VALIDATION] : (state, {payload}) => {
		return Object.assign({}, state, {validation : payload})
	}
}, initialState)