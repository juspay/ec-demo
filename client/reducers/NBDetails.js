import { handleActions } from 'redux-actions'
import {
	SET_NB_OPTIONS,
	SET_SELECTED_BANK,
	SET_SELECTED_BANK_LABEL,
	UPDATE_NB_VALIDATION
} from '../constants/ActionTypes'

const initialState = {
	nbOptions : [],
	selectedBank : '',
	selectedBankLabel : 'Choose a bank',
	validation : {
		selectedBankEmpty : false
	}
}

export default handleActions({
	[SET_NB_OPTIONS] : (state, {payload}) => {
		return Object.assign({}, state, {nbOptions : payload})
	},
	[SET_SELECTED_BANK] : (state, {payload}) => {
		return Object.assign({}, state, {selectedBank : payload})
	},
	[SET_SELECTED_BANK_LABEL] : (state, {payload}) => {
		return Object.assign({}, state, {selectedBankLabel : payload})
	},
	[UPDATE_NB_VALIDATION] : (state, {payload}) => {
		return Object.assign({}, state, {validation : payload})
	}
}, initialState)