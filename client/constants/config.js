export const GLOBAL_TIME_OUT = 3000 //setting timeout for requests as 3s
export const ORDER_CREATE_URL = '/orders/create'
export const LIST_CARDS_URL = '/customers/:customerId/cards'
export const PAYMENT_METHODS_URL = '/merchant/paymentmethods'
export const ORDER_STATUS_URL = '/orders/:orderId'