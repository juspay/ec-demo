import React, { Component } from 'react';
import { connect } from 'react-redux'
import { 
	Dimmer,
	Segment,
	Image,
	Button,
	Card,
	Dropdown
} from 'semantic-ui-react'
import { actions as actions } from '../actions/MainActions'
import JuspayLoader from '../components/JuspayLoader'
import Message from '../components/Message'
import '../styles/WalletPaymentView.css'

const mapStateToProps = (state) => ({
	orderId : state.orderDetails.orderId,
	amount : state.orderDetails.amount,
	supportedPaymentMethods : state.orderDetails.supportedPaymentMethods,
	loader : state.orderDetails.loader,
	walletOptions : state.walletDetails.walletOptions,
	selectedWallet : state.walletDetails.selectedWallet,
	selectedWalletLabel : state.walletDetails.selectedWalletLabel,
	validation : state.walletDetails.validation
})

class WalletPaymentView extends Component {

	constructor(props) {
		super(props)
	}

	resetState = () => {
		this.props.setSelectedWallet('')
		this.props.setSelectedWalletLabel('Choose a wallet')
	}

	componentDidMount() {
		this.props.hideLoader()
		this.resetState()
		if(!this.props.orderId) {
			this.props.syncOrderDetails(this.props.routeParams.orderId)
			this.props.getPaymentMethods()
		}
		this.props.updateWalletValidation({
			selectedWalletEmpty : false
		})
	}

	componentDidUpdate = (prevProps, prevState) => {
		if(!prevProps.orderId && this.props.orderId) {
			this.props.getStoredCards(this.props.customerId)
		}
	}

	handlePayment = (e) => {
		e.preventDefault()
		this.props.showLoader()
		var validation = []
		validation.selectedBankEmpty = false
		if(!this.props.selectedWallet) {
			validation.selectedWalletEmpty = true
			this.props.updateWalletValidation(validation)
			this.props.hideLoader()
		} else {
			document.getElementById('juspay-wallet-payment-method').value = this.props.selectedWallet
			document.getElementById('juspay-wallet-payment-form').submit()
			setTimeout(() => {
				/*
				 * Incase, the form submit fails
				 */
				this.props.hideLoader()
			}, 3000)
		}
	}

	render() {
		var tagline = 'Please select a wallet to proceed. Total amount payable: \u20b9 ' + this.props.amount
		var validationMessage = []
		if(this.props.validation.selectedWalletEmpty) {
			validationMessage.push(
				<p className='param-rule'><b>* Please choose a wallet</b></p>
			)
		}
		return (
			<Dimmer.Dimmable dimmed={this.props.loader} className='juspay-dimmer-loader'>
				<JuspayLoader 
					active={this.props.loader} 
					message='Please do not hit refresh or back button ...'/>
				<Message message={tagline} />
				<div className='wallet-container'>
					{validationMessage}
					<Dropdown 
						text={this.props.selectedWalletLabel} 
						fluid 
						search
						selection 
						options={this.props.walletOptions}
						onChange={(e, data) => {
							this.props.setSelectedWallet(data.value)
							data.options.every((obj, index) => {
								if(obj.value == data.value) {
									this.props.setSelectedWalletLabel(obj.text)
									return false
								}
								return true
							})
						}}/>
					<Segment basic fluid>
						<Card.Group itemsPerRow={3}>
							<Card 
								as='a' 
								raised 
								color={this.props.selectedWallet == 'PAYTM' ? 'teal' : ''}
								onClick={(e) => { 
									this.props.setSelectedWallet('PAYTM') 
									this.props.setSelectedWalletLabel('PayTM Wallet')
								}}>
								<Card.Content >
									<Image src='/static/juspay_paytm.png'/>
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised 
								color={this.props.selectedWallet == 'FREECHARGE' ? 'teal' : ''}
								onClick={(e) => { 
									this.props.setSelectedWallet('FREECHARGE') 
									this.props.setSelectedWalletLabel('Freecharge Wallet')
								}}>
								<Card.Content >
									<Image src='/static/juspay_freecharge.png' />
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised 
								color={this.props.selectedWallet == 'MOBIKWIK' ? 'teal' : ''}
								onClick={(e) => { 
									this.props.setSelectedWallet('MOBIKWIK') 
									this.props.setSelectedWalletLabel('Mobikwik Wallet')
								}}>
								<Card.Content >
									<Image src='/static/juspay_mobikwik.png'/>
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised 
								color={this.props.selectedWallet == 'OLAMONEY' ? 'teal' : ''}
								onClick={(e) => { 
									this.props.setSelectedWallet('OLAMONEY') 
									this.props.setSelectedWalletLabel('Olamoney Wallet')
								}}>
								<Card.Content >
									<Image src='/static/juspay_olamoney.png'/>
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised 
								color={this.props.selectedWallet == 'SBIBUDDY' ? 'teal' : ''}
								onClick={(e) => { 
									this.props.setSelectedWallet('SBIBUDDY') 
									this.props.setSelectedWalletLabel('SBI Buddy Wallet')
								}}>
								<Card.Content >
									<Image src='/static/juspay_sbibuddy.png'/>
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised 
								color={this.props.selectedWallet == 'PAYUMONEY' ? 'teal' : ''}
								onClick={(e) => { 
									this.props.setSelectedWallet('PAYUMONEY') 
									this.props.setSelectedWalletLabel('PayU Money Wallet')
								}}>
								<Card.Content >
									<Image src='/static/juspay_payumoney.png'/>
								</Card.Content>
							</Card>
						</Card.Group>
					</Segment>
					<Button 
						fluid 
						primary 
						content='Make Payment' 
						icon='right arrow' 
						labelPosition='right' 
						onClick={this.handlePayment}/>
				</div>
				<form id='juspay-wallet-payment-form' action='https://sandbox.juspay.in/txns' method='post'>
					<input type='hidden' name='order_id' value={this.props.orderId} />
					<input type='hidden' name='merchant_id' value='ec_demo' />
					<input type='hidden' name='payment_method_type' value='WALLET' />
					<input type='hidden' id='juspay-wallet-payment-method' name='payment_method' value='' />
					<input type='hidden' name='redirect_after_payment' value='true' />
				</form>
			</Dimmer.Dimmable>
		);
	}
}

export default connect(mapStateToProps, actions)(WalletPaymentView)