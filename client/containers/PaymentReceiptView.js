import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { actions as actions } from '../actions/MainActions'
import {
	Segment,
	Card,
	Button,
	Message,
	Icon,
	Container,
	Dimmer
} from 'semantic-ui-react'
import JuspayLoader from '../components/JuspayLoader'
import '../styles/PaymentEndView.css'

const mapStateToProps = (state) => ({
	orderId : state.orderDetails.orderId,
	amount : state.orderDetails.amount,
	status : state.orderDetails.status,
	loader : state.orderDetails.loader
})

const data = {
	"success" : {
		nextButtonLabel : 'New Transaction',
		nextButtonUrl : '/',
		paymentStatusHeader : 'Payment Success',
		iconName : 'check circle',
		iconColor : 'green',
		paymentDesc : 'is successful.',
		paymentReceiptLabel : 'Amount Paid',
		contactMessage : ''
	},
	"failure" : {
		nextButtonLabel : 'Retry Payment',
		nextButtonUrl : '/orders/:orderId/paymentoptions',
		paymentStatusHeader : 'Payment Failed',
		iconName : 'remove circle',
		iconColor : 'red',
		paymentDesc : 'has failed due to some technical error.',
		paymentReceiptLabel : 'Amount to be Paid',
		contactMessage : 'For clarifications contact support@juspay.in'
	}
}

class PaymentReceiptView extends Component {

	constructor(props) {
		super(props)
	}

	componentDidMount = () => {
		this.props.showLoader()
		this.props.syncOrderDetails(this.props.routeParams.orderId)
		this.props.getPaymentMethods()
	}

	render() {
		var status = (['SUCCESS', 'CHARGED'].indexOf(this.props.status) > -1) ? 'success' : 'failure'
		var nextButtonLabel = data[status].nextButtonLabel
		var nextButtonUrl = data[status].nextButtonUrl.replace(':orderId', this.props.orderId)
		var paymentStatusHeader = data[status].paymentStatusHeader
		var iconName = data[status].iconName
		var paymentDesc = data[status].paymentDesc
		var iconColor = data[status].iconColor
		var contactMessage = data[status].contactMessage
		var paymentReceiptLabel = data[status].paymentReceiptLabel
		return (
			<Dimmer.Dimmable as={Container} dimmed={this.props.loader} className='juspay-dimmer-loader'>
				<JuspayLoader 
					active={this.props.loader} 
					message='Verifying payment status. Please wait...'/>
				<div className='payment-end-container' hidden={this.props.loader}>
					<Message className='payment-details-container'>
						<div className={'payment-status-container ' + status}>
							<Icon name={iconName} size='big' color={iconColor}/>{paymentStatusHeader}
						</div>
						<div className={'payment-desc ' + status}>
							Your payment of &#8377; {this.props.amount}, towards transaction ID : {this.props.orderId}, {paymentDesc}
							<div className='payment-end-button'>
								<Button 
									primary 
									content={nextButtonLabel} 
									onClick={(e) => {
										browserHistory.push(nextButtonUrl)
									}}/>
							</div>
							<div className='contact-juspay'>
								{contactMessage}
							</div>
						</div>
					</Message>
					<Segment fluid className='payment-receipt-container'>
						<div className='payment-receipt-header'>
							PAYMENT RECEIPT
						</div>
						<div className='payment-receipt-left'>
							{paymentReceiptLabel}
						</div>
						<div className='payment-receipt-right'>
							{'\u20b9 ' + this.props.amount}
						</div>
					</Segment>
				</div>
			</Dimmer.Dimmable>
		)
	}

}

export default connect(mapStateToProps, actions)(PaymentReceiptView)