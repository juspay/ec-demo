import React, { Component } from 'react';
import { connect } from 'react-redux'
import { actions as actions } from '../actions/MainActions'
import { 
	Dimmer, 
	Container 
} from 'semantic-ui-react'
import Message from '../components/Message'
import JuspayLoader from '../components/JuspayLoader'
import MenuList from '../components/MenuList'

const mapStateToProps = (state) => ({
	orderId : state.orderDetails.orderId,
	amount : state.orderDetails.amount,
	webUrl : state.orderDetails.webUrl,
	mobileUrl : state.orderDetails.mobileUrl,
	iframeUrl : state.orderDetails.iframeUrl,
	loader : state.orderDetails.loader
})

class PaymentLinksView extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount = () => {
		if(!this.props.orderId) {
			this.props.showLoader()
			this.props.syncOrderDetails(this.props.routeParams.orderId)
			this.props.getPaymentMethods()
		}
	}

	render() {
		const items = [{
			content : 'WEB CHECKOUT PAGE',
			url : this.props.webUrl,
			external : true
		}, {
			content : 'MOBILE CHECKOUT PAGE',
			url : this.props.mobileUrl,
			external : true
		}, {
			content : 'IFRAME CHECKOUT PAGE',
			url : this.props.iframeUrl,
			external : true
		}]
		var tagline = 'Please select one of the listed payment links. . Total amount payable: \u20b9 ' + this.props.amount
		return (
			<Dimmer.Dimmable as={Container} dimmed={this.props.loader} className='juspay-dimmer-loader'>
				<JuspayLoader 
					active={this.props.loader} 
					message=''/>
				<Message message={tagline} />
				<MenuList items={items} />
			</Dimmer.Dimmable>
		);
	}
}

export default connect(mapStateToProps, actions)(PaymentLinksView)