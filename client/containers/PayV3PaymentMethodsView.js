import React, { Component } from 'react';
import Message from '../components/Message'
import MenuList from '../components/MenuList'

export default class PayV3PaymentMethodsView extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const items = [{
			content : 'CREDIT/DEBIT CARD',
			url : '/orders/:orderId/pay-v3/pay/card'.replace(':orderId', this.props.routeParams.orderId),
			external : false
		}, {
			content : 'NETBANKING',
			url : '/orders/:orderId/pay-v3/pay/netbanking'.replace(':orderId', this.props.routeParams.orderId),
			external : false
		}, {
			content : 'WALLET',
			url : '/orders/:orderId/pay-v3/pay/wallet'.replace(':orderId', this.props.routeParams.orderId),
			external : false
		}]
		return (
			<div>
				<Message 
					message='Please select a payment method for the transaction' />
				<MenuList 
					items={items} />
			</div>
		);
	}
}
