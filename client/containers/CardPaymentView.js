import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
	Menu, 
	Container,
	Card,
	Input,
	Message as SemanticMessage,
	Button,
	Dropdown,
	Icon,
	Image,
	Dimmer,
	Header,
	Segment
} from 'semantic-ui-react';
import { actions as actions } from '../actions/MainActions'
import CardValidator from '../utils/CardValidator'
import Message from '../components/Message'
import JuspayLoader from '../components/JuspayLoader'
import '../styles/CardPaymentView.css'

class SavedCards extends Component {

	getCardClass = (brand) => {
		if(!brand)
			return 'credit card alternative'
		brand = brand.toLowerCase()
		if(brand.indexOf('visa') > -1) {
			return 'visa'
		} else if(brand.indexOf('master') > -1) {
			return 'mastercard'
		} else if(brand.indexOf('discover') > -1) {
			return 'discover'
		} else if(brand.indexOf('diners') > -1) {
			return 'diners club'
		} else if(brand.indexOf('amex') > -1) {
			return 'american express'
		} else if(brand.indexOf('jcb') > -1) {
			return 'japan credit bureau'
		} else {
			return 'credit card alternative'
		}
	}

	render() {
		var savedCardsList = []
		var cardSecurityCodeMessage = []
		if(this.props.validation) {
			if(this.props.validation.cardSecurityCodeEmpty) {
				cardSecurityCodeMessage.push(
					<p className='param-rule'><b>* CVV Required</b></p>
				)
			} else if(this.props.validation.cardSecurityCodeInvalid) {
				cardSecurityCodeMessage.push(
					<p className='param-rule'><b>* Invalid CVV</b></p>
				)
			}
		}
		savedCardsList.push(cardSecurityCodeMessage)
		if(this.props.cards.length > 0) {
			this.props.cards.forEach((storedCard) => {
				storedCard = JSON.parse(storedCard)
				savedCardsList.push(
					<Card 
						fluid 
						as='a' 
						onClick={(e) => {
							this.props.setCardState('cardToken', storedCard.token)
							this.props.setCardState('cardSecurityCode', '')
							this.props.setCardState('cardBrand', storedCard.brand ? storedCard.brand.toLowerCase() : '')
						}} >
						<Card.Content 
							extra={this.props.cardToken != storedCard.token}>
							<Icon 
								name={this.getCardClass(storedCard.brand)} size='big'/>
							<span className='saved-card-desc'>
								{storedCard.number}
								<Input 
									className='saved-card-cvv' 
									disabled={this.props.cardToken != storedCard.token}
									autoFocus={this.props.cardToken == storedCard.token}
									focus={this.props.cardToken == storedCard.token} 
									placeholder='CVV' 
									type='password'
									maxLength='4'
									onChange={(e) => {
										this.props.setCardState('cardSecurityCode', e.target.value)
									}} />
							</span>
						</Card.Content>
					</Card>
				)
			})
			savedCardsList.push(
				<Button 
					fluid 
					primary 
					content='Make Payment' 
					icon='right arrow' 
					labelPosition='right' 
					onClick={(e) => {
						this.props.handlePayment('saved_card')
					}}/>
			)
		} else {
			savedCardsList.push(
				<Container fluid>
					<SemanticMessage negative icon>
						<Icon name='exclamation circle'/>
						<SemanticMessage.Content>
							<SemanticMessage.Header>
								No saved cards available for the customer.
							</SemanticMessage.Header>
							<p>Please proceed the payment with a new card.</p>
						</SemanticMessage.Content>
					</SemanticMessage>
				</Container>
			)
		}
		if(this.props.active) {
			return (
				<div className='card-container'>
					{savedCardsList}
				</div>
			)
		} else {
			return (
				<div />
			)
		}
	}
}

class NewCard extends Component {

	range (start, count) {
      return Array.apply(0, Array(count))
        .map(function (element, index) { 
          return index + start;  
      })
    }

    getCardClass = (result) => {
	    var cardType = result.card_type;

	    this.isCardValid = result.valid;
	    if (!cardType)
	    	return 'credit card alternative'

	    switch (cardType.name) {
	      	case 'visa': 
	      		return ' visa'
	      	case 'mastercard': 
	      		return ' mastercard'
	     	 case 'amex': 
	      		return 'american express'
	      	case 'discover': 
	      		return ' discover'
	      	case 'jcb' :
	      		return 'japan credit bureau'
	      	case 'diners_club_international':
	      		return 'diners club'
	      	case 'diners_club_carte_blanche':
	      		return 'diners club'
	      	default : 
	      		return 'credit card alternative'
	    }
	  }

	render() {
		if(this.props.active) {
			var expiryMonthList = this.range(1,12).map((x) => {
	             return {
	             	text : x,
	             	value : x
	             }
	        })
	        var expiryYearList = this.range(2017,34).map((x) => {
	        	return {
	        		text : x,
	        		value : x
	        	}
	        })
	        var cardClass = this.getCardClass(CardValidator(this.props.cardDetails.cardNumber))
	        var cardNumberMessage = []
	        var cardExpiryMessage = []
	        var cardSecurityCodeMessage = []
	        if(this.props.validation) {
	        	if(this.props.validation.cardNumberEmpty) {
	        		cardNumberMessage.push(
	        			<p className='param-rule'><b>* Required</b></p>
	        		)
	        	} else if(this.props.validation.cardNumberInvalid) {
	        		cardNumberMessage.push(
	        			<p className='param-rule'><b>* Invalid card number</b></p>
	        		)
	        	}
	        	if(this.props.validation.cardExpiryMonthEmpty || this.props.validation.cardExpiryYearEmpty) {
	        		cardExpiryMessage.push(
	        			<p className='param-rule'><b>* Required</b></p>
	        		)
	        	} else if(this.props.validation.cardExpiryMonthInvalid || this.props.validation.cardExpiryYearInvalid) {
	        		cardExpiryMessage.push(
	        			<p className='param-rule'><b>* Invalid expiry date</b></p>
	        		)
	        	}
	        	if(this.props.validation.cardSecurityCodeEmpty) {
	        		cardSecurityCodeMessage.push(
	        			<p className='param-rule'><b>* Required</b></p>
	        		)
	        	} else if(this.props.validation.cardSecurityCodeInvalid) {
	        		cardSecurityCodeMessage.push(
	        			<p className='param-rule'><b>* Invalid CVV</b></p>
	        		)
	        	}
	        }
			return (
                <div className='card-container'>
                  	<div className='input-block'>
                    	<input 
                    		autofocus 
                    		className='input-area' 
                    		type="tel" 
                    		maxLength="23" 
                    		value={this.props.cardDetails.cardNumber} 
                    		onChange={(e) => {
                    			e.preventDefault()
                    			var cardNumber = e.target.value.replace(/ /g, '');
								if (cardNumber.length > 0){
									cardNumber = cardNumber.match(new RegExp('.{1,4}', 'g')).join(' ');
								}
                    			this.props.setCardState('cardNumber', cardNumber)
                    		}} 
                    		required/>
                    	<div className='input-label'>Card Number</div>
                    	<Icon 
                    		name={cardClass ? cardClass : 'credit card alternative'} 
                    		size='big' 
                    		className='card-icon' 
                    		color='teal'/>
                    	{cardNumberMessage}
                	</div>
	                <div className='month-year-class'>
		                <div className='card-container-label'>
		                	Expiry Date
		                </div>
	                	<Dropdown 
	                		placeholder='MM' 
	                		className='expiry-dropdown' 
	                		selection 
	                		scrolling 
	                		compact
	                		options={expiryMonthList}
	                		onChange={(e,data) => {
	                			this.props.setCardState('cardExpiryMonth', data.value)
	                		}} />
	                	<Dropdown 
	                		placeholder='YYYY' 
	                		className='expiry-dropdown' 
	                		selection 
	                		scrolling 
	                		compact
	                		options={expiryYearList} 
	                		onChange={(e,data) => {
	                			this.props.setCardState('cardExpiryYear', data.value)
	                		}}/>
	                	{cardExpiryMessage}
		            </div>
		            <div className='cvv-class' >
	                    <input 
	                    	type="password" 
	                    	className='input-area' 
	                    	maxLength="4" 
	                    	name="cvv" 
	                    	onChange={(e)=>{
	                    		this.props.setCardState('cardSecurityCode', e.target.value)
	                    	}} 
	                    	required/>
	                    <div className='input-label'>CVV</div>
	                    {cardSecurityCodeMessage}
	                </div>
	                <div className='card-container-label'>
				   		<input type="checkbox" onChange = {(e) => {
				   			this.props.setCardState('saveToLocker', e.target.checked)
				   		}} /> Save card information
					</div>
	                <Button 
						fluid 
						primary 
						content='Make Payment' 
						icon='right arrow' 
						labelPosition='right' 
						onClick={(e) => {
							this.props.handlePayment('new_card')
						}}/>
	            </div>
			)
		} else {
			return (
				<div />
			)
		}
	}
}

const mapStateToProps = (state) => ({
	orderId : state.orderDetails.orderId,
	amount : state.orderDetails.amount,
	customerId : state.orderDetails.customerId,
	loader : state.orderDetails.loader,
	activeTab : state.cardDetails.activeTab,
	storedCards : state.cardDetails.storedCards,
	cardToken : state.cardDetails.cardToken,
	cardNumber : state.cardDetails.cardNumber,
	cardExpiryMonth : state.cardDetails.cardExpiryMonth,
	cardExpiryYear : state.cardDetails.cardExpiryYear,
	cardSecurityCode : state.cardDetails.cardSecurityCode,
	saveToLocker : state.cardDetails.saveToLocker,
	cardBrand : state.cardDetails.cardBrand,
	storedCardsRetrieved : state.cardDetails.storedCardsRetrieved,
	cardValidation : state.cardDetails.cardValidation
})

class CardPaymentView extends Component {

	constructor(props) {
		super(props)
	}

	componentDidUpdate = (prevProps, prevState) => {
		if(!prevProps.orderId && this.props.orderId) {
			this.props.getStoredCards(this.props.customerId)
		}
	}

	componentDidMount = () => {
		this.props.hideLoader()
		if(!this.props.orderId) {
			this.props.syncOrderDetails(this.props.routeParams.orderId)
			this.props.getPaymentMethods()
		}
		this.resetValidation()
		if(!this.props.storedCardsRetrieved) {
			this.props.getStoredCards(this.props.customerId)
		}
	}

	resetValidation = () => {
		var validation = {
			cardExpiryMonthEmpty : false,
			cardExpiryYearEmpty : false,
			cardSecurityCodeEmpty : false,
			cardNumberEmpty : false,
			cardExpiryMonthInvalid : false,
			cardExpiryYearInvalid : false,
			cardSecurityCodeInvalid : false,
			cardNumberInvalid : false
		}
		this.setCardState('cardValidation', validation)
	}

	handleClick = (view) => {
		this.props.setActiveTab(view)
		this.resetValidation()
	}

	setCardState = (key, value) => {
		switch(key) {
			case 'cardNumber' : 
				this.props.updateCardNumber(value);
				break;
			case 'cardExpiryMonth' : 
				this.props.updateCardExpiryMonth(value);
				break;
			case 'cardExpiryYear' : 
				this.props.updateCardExpiryYear(value);
				break;
			case 'cardSecurityCode' : 
				this.props.updateCardSecurityCode(value);
				break;
			case 'cardToken' : 
				this.props.updateCardToken(value);
				break;
			case 'cardValidation' : 
				this.props.updateCardValidation(value);
				break;
			case 'saveToLocker' : 
				this.props.updateSaveToLocker(value);
				break;
			default :
				return;
		}
	}

	getCvvLength = (brand) => {
		if(!brand)
			return [3]
		if(brand.indexOf('maestro')) {
			return [0,3]
		} else if(brand.indexOf('amex')) {
			return [4]
		} else {
			return [3]
		}
	}

	invalidCvv = (brand, cvv) => {
	    if(!cvv)
	    	return false
	    return (this.getCvvLength(brand).indexOf(cvv.length) < 0 || isNaN(cvv))
	  }

	isValid = (type) => {
		if(type == 'new_card') {
			var result = true;
		    var validation = {};
		    var re = /^[0-3][0-9]\/[0-9]{2}$/;
		    validation.cardNumberEmpty = false;
		    validation.cardExpiryMonthEmpty = false;
		    validation.cardExpiryYearEmpty = false;
		    validation.cardSecurityCodeEmpty = false;
		    validation.cardNumberInvalid = false;
		    validation.cardExpiryMonthInvalid = false;
		    validation.cardExpiryYearInvalid = false;
		    validation.cardSecurityCodeInvalid = false;

		    var date = new Date()
		    var currentMonth = date.getMonth() + 1
		    var currentYear = date.getFullYear()

		    if(!this.props.cardNumber) {
		    	validation.cardNumberEmpty = true;
		    	result = false;
		    }
		    if(this.props.cardNumber && !CardValidator(this.props.cardNumber).valid) {
		    	validation.cardNumberInvalid  = true;
		    	result = false;
		    }
		    if(!this.props.cardExpiryMonth) {
		    	validation.cardExpiryMonthEmpty = true;
		    	result = false;
		    }
		    if(this.props.cardExpiryMonth && isNaN(this.props.cardExpiryMonth)) {
		    	validation.cardExpiryMonthInvalid = true;
		    	result = false;
		    }
		    if(!this.props.cardExpiryYear) {
		    	validation.cardExpiryYearEmpty = true;
		    	result = false;
		    }
		    if(this.props.cardExpiryYear && isNaN(this.props.cardExpiryYear)) {
		    	validation.cardExpiryYearInvalid  = true;
		    	result = false;
		    }
		    if(this.props.cardExpiryYear && this.props.cardExpiryMonth && (Number.parseInt(this.props.cardExpiryYear) == currentYear
		      && Number.parseInt(this.props.cardExpiryMonth) < currentMonth )) {
		    	validation.cardExpiryMonthInvalid  = true;
		    	result = false;
		    }
		    var cardClass = CardValidator(this.props.cardNumber)
		    if(cardClass && cardClass.card_type && cardClass.card_type.name == 'maestro') {
		    	if(this.props.cardSecurityCode && this.invalidCvv('maestro', this.props.cardSecurityCode)) {
		      		validation.cardSecurityCodeInvalid = true;
		        	result = false;
		      	}
		    } else {
		    	if(!this.props.cardSecurityCode) {
		        	validation.cardSecurityCodeEmpty = true;
		        	result = false;
		      	}
		    	if(this.props.cardSecurityCode && this.invalidCvv(cardClass && cardClass.card_type &&
		          cardClass.card_type.name || null, this.props.cardSecurityCode)) {
		      		validation.cardSecurityCodeInvalid = true;
		       		result = false;
		      	}
		    }
		    this.setCardState('cardValidation', validation)
		    return result
		} else if(type == 'saved_card') {
			var result = true
			var validation = {}
			validation.cardSecurityCodeEmpty = false
			validation.cardSecurityCodeInvalid = false
			if(!this.props.cardSecurityCode) {
				validation.cardSecurityCodeEmpty = true
				result = false
			}
			if(this.invalidCvv(this.props.cardBrand, this.props.cardSecurityCode)) {
				validation.cardSecurityCodeInvalid = true
				result = false
			}
			this.setCardState('cardValidation', validation)
			return result
		}
	}

	handlePayment = (type) => {
		this.props.showLoader()
		if(!this.isValid(type)) {
			this.props.hideLoader()
			return
		} else {
			if(type == 'new_card') {
				document.getElementById('juspay-card-number').value = this.props.cardNumber
				document.getElementById('juspay-card-exp-month').value = this.props.cardExpiryMonth
				document.getElementById('juspay-card-exp-year').value = this.props.cardExpiryYear
				document.getElementById('juspay-save-to-locker').value = this.props.saveToLocker
			} else if(type == 'saved_card') {
				document.getElementById('juspay-card-token').value = this.props.cardToken
			}
			document.getElementById('juspay-card-security-code').value = this.props.cardSecurityCode
			document.getElementById('juspay-payment-form').submit()
		}
	}

	render() {
		var cardDetails = {
			cardNumber : this.props.cardNumber,
			cardExpiryMonth : this.props.cardExpiryMonth,
			cardExpiryYear : this.props.cardExpiryYear,
			cardSecurityCode : this.props.cardSecurityCode
		}
		var tagline = 'Please enter your card details to proceed. Total amount payable: \u20b9 ' + this.props.amount
		return (
			<Dimmer.Dimmable dimmed={this.props.loader} className='juspay-dimmer-loader'>
				<JuspayLoader 
					active={this.props.loader} 
					message='Please do not hit refresh or back button ...'/>
				<Message 
					message={tagline} />
				<Menu 
					pointing 
					secondary 
					fluid 
					widths={2} 
					color='blue'>
					<Menu.Item 
						name='Saved Cards' 
						active={this.props.activeTab != 'new_card'} 
						onClick={() => { this.handleClick('saved_cards') }} />
					<Menu.Item 
						name='New Card' 
						active={this.props.activeTab == 'new_card'} 
						onClick={() => { this.handleClick('new_card') }} />
				</Menu>
				<SavedCards 
					active={this.props.activeTab != 'new_card'} 
					cards={this.props.storedCards} 
					cardToken={this.props.cardToken} 
					validation={this.props.cardValidation}
					handlePayment={this.handlePayment} 
					setCardState={this.setCardState}
					loader={this.props.loader} />
				<NewCard 
					active={this.props.activeTab == 'new_card'} 
					cardDetails={cardDetails} 
					validation={this.props.cardValidation}
					handlePayment={this.handlePayment} 
					setCardState={this.setCardState}
					loader={this.props.loader} />
				<form id='juspay-payment-form' action='https://sandbox.juspay.in/txns' method='post'>
					<input type='hidden' name='order_id' value={this.props.orderId} />
					<input type='hidden' name='merchant_id' value='ec_demo' />
					<input type='hidden' name='payment_method_type' value='CARD' />
					<input type='hidden' id='juspay-card-token' name='card_token' value='' />
					<input type='hidden' id='juspay-card-number' name='card_number' value='' />
					<input type='hidden' id='juspay-card-exp-year' name='card_exp_year' value='' />
					<input type='hidden' id='juspay-card-exp-month' name='card_exp_month' value='' />
					<input type='hidden' id='juspay-card-security-code' name='card_security_code' value='' />
					<input type='hidden' id='juspay-save-to-locker' name='save_to_locker' value='' />
					<input type='hidden' name='redirect_after_payment' value='true' />
				</form>
			</Dimmer.Dimmable>
		);
	}
}

export default connect(mapStateToProps, actions)(CardPaymentView)
