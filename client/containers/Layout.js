import React, { Component } from 'react'
import { connect } from 'react-redux';
import Header from '../components/Header'
import Footer from '../components/Footer'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import '../styles/core.css'


const mapStateToProps = (state) => ({
	orderId : state.orderDetails.orderId
})

class Layout extends Component {

	constructor(props) {
		super(props)
	}

	render() {
		return (
			<div className='layout'>
				<Header location={this.props.location} orderId={this.props.orderId}/>
				<div className='app'>
					<ReactCSSTransitionGroup
			            component="div"
			            transitionName="example"
			            transitionEnterTimeout={150}
			            transitionLeaveTimeout={200}>

			            {React.cloneElement(this.props.children, {
			              key: this.props.location.pathname
			            })}
			        </ReactCSSTransitionGroup>
				</div>
				<Footer />
			</div>
		);
	}
}

export default connect(mapStateToProps)(Layout)