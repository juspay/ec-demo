import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import {
	actions as actions
} from '../actions/MainActions'
import {
	Menu,
	Dimmer,
	Button,
	Icon,
	Message as SemanticMessage,
	Card,
	Container
} from 'semantic-ui-react'
import Message from '../components/Message'
import JuspayLoader from '../components/JuspayLoader'
import '../styles/PayV3CardPaymentView.css'

class SavedCards extends Component {
	constructor(props) {
		super(props)
	}

	getCardClass = (brand) => {
		if(!brand)
			return 'credit card alternative'
		brand = brand.toLowerCase()
		if(brand.indexOf('visa') > -1) {
			return 'visa'
		} else if(brand.indexOf('master') > -1) {
			return 'mastercard'
		} else if(brand.indexOf('discover') > -1) {
			return 'discover'
		} else if(brand.indexOf('diners') > -1) {
			return 'diners club'
		} else if(brand.indexOf('amex') > -1) {
			return 'american express'
		} else if(brand.indexOf('jcb') > -1) {
			return 'japan credit bureau'
		} else {
			return 'credit card alternative'
		}
	}

	handlePayment = (e) => {
		this.props.showLoader()
		var formId = '#saved_card_' + this.props.cardToken
		$(formId).submit()
	}

	componentDidMount = () => {
		if(this.props.cards.length > 0) {
			this.props.cards.forEach((storedCard) => {
				storedCard = JSON.parse(storedCard)
				Juspay.Setup({
					payment_form : '#saved_card_' + storedCard.token,
					success_handler : (status) => {
						this.props.showLoader()
			        	var url = '/orders/' + this.props.orderId + '/receipt'
			        	browserHistory.push(url)
					},
					error_handler : (error_code, error_message, bank_error_code, bank_error_message, gateway_id) => {
						this.props.showLoader()
			        	var url = '/orders/' + this.props.orderId + '/receipt'
			        	browserHistory.push(url)
					},
					iframe_elements : {
						security_code : {
							container : ".security-code-div",
							attributes : {
								placeholder : "CVV"
							}
						}
					},
					styles : {
						"input" : {
							"border": "none !important",
						    "display": "block !important",
						    "width": "100% !important",
						    "outline": "none !important",
						    "height": "40px !important",
						    "padding": "5px !important",
						    "border-bottom": "1px solid #C3C3C3 !important",
						    "letter-spacing": "1px  !important",
						    "margin-bottom": "5px !important",
						    "font-size": "14px !important"
						},
						":focus" : {
							"border-color" : "#0585DD !important"
						},
						".security_code" : {
							"max-width" : "70px !important",
							"display" : "inline-block !important",
							"max-height" : "40px !important"
						}
					},
					iframe_element_callback : (event) => {
						if(this.props.cardToken != storedCard.token) {
							this.props.setCardState('cardToken', storedCard.token)
						}
						var validation = this.props.validation
						if(event.target_element == 'security_code') {
							validation.cardSecurityCodeEmpty = event.empty
							validation.cardSecurityCodeInvalid = !event.valid
						}
						this.props.setCardState('cardValidation', Object.assign({}, this.props.validation, validation))
					}
				})
			})
		}
	}

	render() {
		var savedCardsList = []
		var cardSecurityCodeMessage = []
		if(this.props.validation) {
			if(this.props.validation.cardSecurityCodeEmpty) {
				cardSecurityCodeMessage.push(
					<p className='param-rule'><b>* CVV Required</b></p>
				)
			} else if(this.props.validation.cardSecurityCodeInvalid) {
				cardSecurityCodeMessage.push(
					<p className='param-rule'><b>* Invalid CVV</b></p>
				)
			}
		}
		if(this.props.cards.length > 0) {
			this.props.cards.forEach((storedCard) => {
				storedCard = JSON.parse(storedCard)
				savedCardsList.push(
					<Card 
						fluid 
						as='a' 
						onClick={(e) => {
							this.props.setCardState('cardToken', storedCard.token)
							this.props.setCardState('cardSecurityCode', '')
							this.props.setCardState('cardBrand', storedCard.brand ? storedCard.brand.toLowerCase() : '')
						}} >
						<Card.Content 
							extra={this.props.cardToken != storedCard.token}>
							<div className='payv3-saved-card-container'>
								<form className='juspay_inline_form' id={'saved_card_' + storedCard.token}>
									<Icon 
									name={this.getCardClass(storedCard.brand)} size='big'/>
									<span className='payv3-saved-card-desc'>
										{storedCard.number}
									</span>
									<input type='hidden' className='merchant_id' value='ec_demo'/>
									<input type='hidden' className='order_id' value={this.props.orderId} />
									<input type='hidden' className='card_token' value={storedCard.token} />
									<input type='hidden' className='card_isin' value={storedCard.isin} />
									<div className='payv3-saved-cvv'>
										<div className="security-code-div" />
									</div>
									<input type='hidden' className='redirect' value='false' />
								</form>
							</div>
						</Card.Content>
					</Card>
				)
			})
			savedCardsList.push(cardSecurityCodeMessage)
			savedCardsList.push(
				<button 
				type="submit" 
				id="common_pay_btn" 
				className="make_payment ui fluid icon primary right labeled button"
				onClick={(e) => {
					this.handlePayment()
				}}>
					Make Payment
				</button>
			)
		} else {
			savedCardsList.push(
				<Container fluid>
					<SemanticMessage negative icon>
						<Icon name='exclamation circle'/>
						<SemanticMessage.Content>
							<SemanticMessage.Header>
								No saved cards available for the customer.
							</SemanticMessage.Header>
							<p>Please proceed the payment with a new card.</p>
						</SemanticMessage.Content>
					</SemanticMessage>
				</Container>
			)
		}
		if(this.props.active) {
			return (
				<div className='payv3-card-container'>
					{savedCardsList}
				</div>
			)
		} else {
			return (
				<div />
			)
		}
	}
}

class NewCard extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount = () => {
		Juspay.Setup({
			payment_form : "#juspay-new-card-form",
			success_handler : (status) => {
				this.props.showLoader()
	        	var url = '/orders/' + this.props.orderId + '/receipt'
	        	browserHistory.push(url)
			},
			error_handler : (error_code, error_message, bank_error_code, bank_error_message, gateway_id) => {
				this.props.showLoader()
	        	var url = '/orders/' + this.props.orderId + '/receipt'
	        	browserHistory.push(url)
			},
			iframe_elements : {
				card_number : {
					container : ".card-number-div",
					attributes : {
						placeholder : "Card Number"
					}
				},
				card_exp_month : {
					container : ".card-exp-month-div",
					attributes : {
						placeholder : "MM"
					}
				},
				card_exp_year : {
					container : ".card-exp-year-div",
					attributes : {
						placeholder : "YY"
					}
				},
				security_code : {
					container : ".security-code-div",
					attributes : {
						placeholder : "CVV",
						maxlength : "4"
					}
				},
				name_on_card : {
					container : ".name-on-card-div",
					attributes : {
						placeholder : "Name on Card"
					}
				}
			},
			styles : {
				"input" : {
					"border": "none !important",
				    "display": "block !important",
				    "width": "100% !important",
				    "outline": "none !important",
				    "height": "40px !important",
				    "padding": "5px !important",
				    "border-bottom": "1px solid #C3C3C3 !important",
				    "letter-spacing": "1px  !important",
				    "margin-bottom": "5px !important",
				    "font-size": "14px !important"
				},
				".card_exp_month" : {
					"max-width" : "70px !important",
					"margin-right" : "5px !important",
					"display" : "inline-block !important"
				},
				".card_exp_year" : {
					"max-width" : "70px !important",
					"margin-right" : "5px !important",
					"display" : "inline-block !important"
				},
				":focus" : {
					"border-color" : "#0585DD !important"
				}
			},
			iframe_element_callback : (event) => {
				var validation = this.props.validation
				switch(event.target_element) {
					case "card_number" :
						validation.cardNumberEmpty = event.empty
						validation.cardNumberInvalid = !event.valid
						var cardClass = this.getCardClass(event.card_brand)
						this.props.setCardState('cardBrand', cardClass)
						break;
					case "card_exp_month" :
						validation.cardExpiryMonthEmpty = event.empty
						validation.cardExpiryMonthInvalid = !event.expiry_valid
						validation.cardExpiryYearInvalid = !event.expiry_valid
						break;
					case "card_exp_year" :
						validation.cardExpiryYearInvalid = !event.expiry_valid
						validation.cardExpiryMonthInvalid = !event.expiry_valid
						validation.cardExpiryYearEmpty = event.empty
						break;
					case "security_code" :
						validation.cardSecurityCodeInvalid = !event.valid
						validation.cardSecurityCodeEmpty = event.empty
						break;
					case 'name_on_card' : 
						validation.nameOnCardInvalid = !event.valid
						validation.nameOnCardEmpty = event.empty
						break;
				}
				this.props.setCardState('cardValidation', Object.assign({}, this.props.validation, validation))
			}
		})
	}

	getCardClass = (brand) => {
	    if (!brand)
	    	return 'credit card alternative'
	    brand = brand.toLowerCase()
		if(brand.indexOf('visa') > -1) {
			return 'visa'
		} else if(brand.indexOf('master') > -1) {
			return 'mastercard'
		} else if(brand.indexOf('discover') > -1) {
			return 'discover'
		} else if(brand.indexOf('diners') > -1) {
			return 'diners club'
		} else if(brand.indexOf('amex') > -1) {
			return 'american express'
		} else if(brand.indexOf('jcb') > -1) {
			return 'japan credit bureau'
		} else {
			return 'credit card alternative'
		}
	}

	render() {
		var cardNumberMessage = []
		var cardExpiryMessage = []
		var cardSecurityCodeMessage = []
		var nameOnCardMessage = []
		if(this.props.validation) {
        	if(this.props.validation.cardNumberEmpty) {
        		cardNumberMessage.push(
        			<p className='param-rule'><b>* Required</b></p>
        		)
        	} else if(this.props.validation.cardNumberInvalid) {
        		cardNumberMessage.push(
        			<p className='param-rule'><b>* Invalid card number</b></p>
        		)
        	}
        	if(this.props.validation.nameOnCardEmpty) {
        		nameOnCardMessage.push(
        			<p className='param-rule'><b>* Required</b></p>
        		)
        	} else if(this.props.validation.nameOnCardInvalid) {
        		nameOnCardMessage.push(
        			<p className='param-rule'><b>* Invalid Name</b></p>
        		)
        	}
        	if(this.props.validation.cardExpiryMonthEmpty || this.props.validation.cardExpiryYearEmpty) {
        		cardExpiryMessage.push(
        			<p className='param-rule'><b>* Required</b></p>
        		)
        	} else if(this.props.validation.cardExpiryMonthInvalid || this.props.validation.cardExpiryYearInvalid) {
        		cardExpiryMessage.push(
        			<p className='param-rule'><b>* Invalid expiry date</b></p>
        		)
        	}
        	if(this.props.validation.cardSecurityCodeEmpty) {
        		cardSecurityCodeMessage.push(
        			<p className='param-rule'><b>* Required</b></p>
        		)
        	} else if(this.props.validation.cardSecurityCodeInvalid) {
        		cardSecurityCodeMessage.push(
        			<p className='param-rule'><b>* Invalid CVV</b></p>
        		)
        	}
        }
		if(this.props.active) {
			return (
				<div className='payv3-card-container'>
					<form className='juspay_inline_form' id='juspay-new-card-form'>
						<input type='hidden' className='merchant_id' value='ec_demo' />
						<input type='hidden' className='order_id' value={this.props.orderId} />
						<div className='payv3-card-number'>
							<div className='card-number-div' />
						</div>
						<Icon 
                    		name={this.props.cardBrand} 
                    		size='big' 
                    		className='payv3-card-icon' 
                    		color='teal'/>
                    	{cardNumberMessage}
                    	<div className='payv3-card-number'>
							<div className='name-on-card-div' />
						</div>
						{nameOnCardMessage}
						<div className='card-container-label'>
		                	Expiry Date
		                </div>
						<div className='exp-cvv-container'>
							<div className='payv3-exp'>
								<div className="card-exp-month-div"></div>
								 <div className="card-exp-year-div"></div>
								{cardExpiryMessage}
							</div>
							<div className='payv3-cvv'>
								<div className='security-code-div' />
									{cardSecurityCodeMessage}
							</div>
						</div>
						<div className='card-container-label'>
					   		<input type="checkbox" onChange = {(e) => {
					   			this.props.setCardState('saveToLocker', e.target.checked)
					   		}} /> Save card information
						</div>
						<input type='hidden' id='juspay-save-to-locker' className='save_to_locker' value={this.props.saveToLocker} />
						<input type='hidden' className='redirect' value='false' />
						<button type="submit" id="common_pay_btn" className="make_payment ui fluid icon primary right labeled button">
							Make Payment
						</button>
					</form>
				</div>
			)
		} else {
			return (
				<div />
			)
		}
	}
}

const mapStateToProps = (state) => ({
	orderId : state.orderDetails.orderId,
	amount : state.orderDetails.amount,
	customerId : state.orderDetails.customerId,
	loader : state.orderDetails.loader,
	activeTab : state.cardDetails.activeTab,
	storedCards : state.cardDetails.storedCards,
	storedCardsRetrieved : state.cardDetails.storedCardsRetrieved,
	cardNumber : state.cardDetails.cardNumber,
	cardExpiryMonth : state.cardDetails.cardExpiryMonth,
	cardExpiryYear : state.cardDetails.cardExpiryYear,
	cardSecurityCode : state.cardDetails.cardSecurityCode,
	saveToLocker : state.cardDetails.saveToLocker,
	cardToken : state.cardDetails.cardToken,
	cardValidation : state.cardDetails.cardValidation,
	cardBrand : state.cardDetails.cardBrand
})

class PayV3CardPaymentView extends Component {

	constructor(props) {
		super(props)
	}

	resetValidation = () => {
		var validation = {
			cardExpiryMonthEmpty : false,
			cardExpiryYearEmpty : false,
			cardSecurityCodeEmpty : false,
			cardNumberEmpty : false,
			nameOnCardEmpty : false,
			cardExpiryMonthInvalid : false,
			cardExpiryYearInvalid : false,
			cardSecurityCodeInvalid : false,
			cardNumberInvalid : false,
			nameOnCardInvalid : false
		}
		this.setCardState('cardValidation', Object.assign({}, this.props.cardValidation, validation))
	}

	setCardState = (key, value) => {
		switch(key) {
			case 'cardNumber' : 
				this.props.updateCardNumber(value);
				break;
			case 'cardExpiryMonth' : 
				this.props.updateCardExpiryMonth(value);
				break;
			case 'cardExpiryYear' : 
				this.props.updateCardExpiryYear(value);
				break;
			case 'cardSecurityCode' : 
				this.props.updateCardSecurityCode(value);
				break;
			case 'cardToken' : 
				this.props.updateCardToken(value);
				break;
			case 'cardValidation' : 
				this.props.updateCardValidation(value);
				break;
			case 'saveToLocker' : 
				this.props.updateSaveToLocker(value);
				break;
			case 'cardBrand' : 
				this.props.updateCardBrand(value);
				break;
			default :
				return;
		}
	}

	handleClick = (view) => {
		this.resetValidation()
		this.props.setActiveTab(view)
	}

	componentDidMount = () => {
		this.props.hideLoader()
		if(!this.props.orderId) {
			this.props.syncOrderDetails(this.props.routeParams.orderId)
			this.props.getPaymentMethods()
		}
		this.resetValidation()
		if(!this.props.storedCardsRetrieved) {
			this.props.getStoredCards(this.props.customerId)
		}
	}

	componentDidUpdate = (prevProps, prevState) => {
		if(!prevProps.orderId && this.props.orderId) {
			this.props.getStoredCards(this.props.customerId)
		}
	}

	render() {
		var tagline = 'Please enter your card details to proceed. Total amount payable: \u20b9 ' + this.props.amount
		return (
			<Dimmer.Dimmable dimmed={this.props.loader} className='juspay-dimmer-loader'>
				<JuspayLoader 
					active={this.props.loader} 
					message='Please do not hit refresh or back button ...'/>
				<Message 
					message={tagline} />
				<Menu 
					pointing 
					secondary 
					fluid 
					widths={2} 
					color='blue'>
					<Menu.Item 
						name='Saved Cards' 
						active={this.props.activeTab != 'new_card'} 
						onClick={() => { this.handleClick('saved_cards') }} />
					<Menu.Item 
						name='New Card' 
						active={this.props.activeTab == 'new_card'} 
						onClick={() => { this.handleClick('new_card') }} />
				</Menu>
				{this.props.activeTab !='new_card' ? 
								<SavedCards 
									active={this.props.activeTab != 'new_card'} 
									cards={this.props.storedCards} 
									cardToken={this.props.cardToken} 
									setCardState={this.setCardState}
									loader={this.props.loader}
									validation={this.props.cardValidation}
									orderId={this.props.orderId}
									showLoader={this.props.showLoader}
									hideLoader={this.props.hideLoader} />
									:
								<NewCard 
									active={this.props.activeTab == 'new_card'} 
									cardBrand={this.props.cardBrand} 
									setCardState={this.setCardState}
									loader={this.props.loader}
									validation={this.props.cardValidation}
									showLoader={this.props.showLoader}
									hideLoader={this.props.hideLoader}
									saveToLocker={this.props.saveToLocker} 
									orderId={this.props.orderId}/>
				}
			</Dimmer.Dimmable>
		);
	}
}

export default connect(mapStateToProps, actions)(PayV3CardPaymentView)