import React, { Component } from 'react';
import { connect } from 'react-redux'
import Message from '../components/Message'

const mapStateToProps = (state) => ({
	orderId : state.orderDetails.orderId,
	amount : state.orderDetails.amount
})

class IFramePaymentView extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		var tagline = 'Please select a payment option to proceed. Total amount payable: \u20b9 ' + this.props.amount
		var iFrameUrl = "https://sandbox.juspay.in/merchant/ipay?order_id=" + this.props.orderId + "&merchant_id=ec_demo&payment_options=card|nb|wallet"
		return (
			<div>
				<Message message={tagline} />
				<iframe 
					id='juspay-iframe'
					src={iFrameUrl} 
					width="100%" 
					style={{border : 'none', height: 'auto', minHeight : '400px'}}/>
			</div>
		);
	}
}

export default connect(mapStateToProps)(IFramePaymentView)
