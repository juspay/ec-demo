import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import {
	Dimmer,
	Segment,
	Image,
	Button,
	Card,
	Dropdown
} from 'semantic-ui-react'
import { actions as actions } from '../actions/MainActions'
import JuspayLoader from '../components/JuspayLoader'
import Message from '../components/Message'
import '../styles/NetBankingPaymentView.css'


const mapStateToProps = (state) => ({
	orderId : state.orderDetails.orderId,
	amount : state.orderDetails.amount,
	supportedPaymentMethods : state.orderDetails.supportedPaymentMethods,
	loader : state.orderDetails.loader,
	nbOptions : state.nbDetails.nbOptions,
	selectedBank : state.nbDetails.selectedBank,
	selectedBankLabel : state.nbDetails.selectedBankLabel,
	validation : state.nbDetails.validation
})

class PayV3NetBankingPaymentView extends Component {

	constructor(props) {
		super(props)
	}

	resetState = () => {
		this.props.setSelectedBank('')
		this.props.setSelectedBankLabel('Choose a bank')
	}

	componentDidUpdate = (prevProps, prevState) => {
		if(!prevProps.orderId && this.props.orderId) {
			this.props.getStoredCards(this.props.customerId)
		}
	}

	componentDidMount() {
		this.props.hideLoader()
		this.resetState()
		if(!this.props.orderId) {
			this.props.syncOrderDetails(this.props.routeParams.orderId)
			this.props.getPaymentMethods()
		}
		this.props.updateNBValidation({
			selectedBankEmpty : false
		})
		Juspay.Setup({
	        payment_form: "#juspay-payv3-nb-payment-form",
	        success_handler: (status) => {
	        	this.props.hideLoader()
	        	var url = '/orders/' + this.props.orderId + '/receipt'
	        	browserHistory.push(url)
	        },
	        error_handler: (error_code, error_message, bank_error_code, bank_error_message, gateway_id) => {
	        	this.props.hideLoader()
	        	var url = '/orders/' + this.props.orderId + '/receipt'
	        	browserHistory.push(url)
	        }
	    })
	}

	handlePayment = (e) => {
		e.preventDefault()
		this.props.showLoader()
		var validation = []
		validation.selectedBankEmpty = false
		if(!this.props.selectedBank) {
			validation.selectedBankEmpty = true
			this.props.updateNBValidation(validation)
			this.props.hideLoader()
		} else {
			document.getElementById('juspay-payv3-submit').click()
		}
	}

	render() {
		var tagline = 'Please select your bank to proceed. Total amount payable: \u20b9 ' + this.props.amount
		var validationMessage = []
		if(this.props.validation.selectedBankEmpty) {
			validationMessage.push(
				<p className='param-rule'><b>* Please choose a bank</b></p>
			)
		}
		return (
			<Dimmer.Dimmable dimmed={this.props.loader} className='juspay-dimmer-loader'>
				<JuspayLoader 
					active={this.props.loader} 
					message='Please do not hit refresh or back button ...'/>
				<Message message={tagline} />
				<div className='nb-container'>
					{validationMessage}
					<Dropdown 
						text={this.props.selectedBankLabel} 
						fluid 
						search 
						selection 
						options={this.props.nbOptions}
						onChange={(e, data) => {
							this.props.setSelectedBank(data.value)
							data.options.every((obj, index) => {
								if(obj.value == data.value) {
									this.props.setSelectedBankLabel(obj.text)
									return false
								}
								return true
							})
						}}/>
					<Segment basic fluid>
						<Card.Group itemsPerRow={3}>
							<Card 
								as='a' 
								raised
								color={ this.props.selectedBank == 'NB_ICICI' ? 'teal' : '' }
								onClick={(e) => { 
									this.props.setSelectedBank('NB_ICICI') 
									this.props.setSelectedBankLabel('ICICI Netbanking')
								}}>
								<Card.Content >
									<Image src='/static/juspay_icici.png'/>
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised
								color={ this.props.selectedBank == 'NB_HDFC' ? 'teal' : '' }
								onClick={(e) => { 
									this.props.setSelectedBank('NB_HDFC') 
									this.props.setSelectedBankLabel('HDFC Bank')
								}}>
								<Card.Content >
									<Image src='/static/juspay_hdfc.png' />
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised
								color={ this.props.selectedBank == 'NB_AXIS' ? 'teal' : '' }
								onClick={(e) => { 
									this.props.setSelectedBank('NB_AXIS') 
									this.props.setSelectedBankLabel('Axis Bank')
								}}>
								<Card.Content >
									<Image src='/static/juspay_axis.png'/>
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised
								color={ this.props.selectedBank == 'NB_KOTAK' ? 'teal' : '' }
								onClick={(e) => { 
									this.props.setSelectedBank('NB_KOTAK') 
									this.props.setSelectedBankLabel('Kotak Bank')
								}}>
								<Card.Content >
									<Image src='/static/juspay_kotak.png'/>
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised
								color={ this.props.selectedBank == 'NB_CITI' ? 'teal' : '' }
								onClick={(e) => { 
									this.props.setSelectedBank('NB_CITI') 
									this.props.setSelectedBankLabel('Citi Bank Netbanking')
								}}>
								<Card.Content >
									<Image src='/static/juspay_citi.png'/>
								</Card.Content>
							</Card>
							<Card 
								as='a' 
								raised
								color={ this.props.selectedBank == 'NB_SBI' ? 'teal' : '' }
								onClick={(e) => { 
									this.props.setSelectedBank('NB_SBI') 
									this.props.setSelectedBankLabel('State Bank of India')
								}}>
								<Card.Content >
									<Image src='/static/juspay_sbi.png'/>
								</Card.Content>
							</Card>
						</Card.Group>
					</Segment>
					<Button 
						fluid 
						primary 
						content='Make Payment' 
						icon='right arrow' 
						labelPosition='right' 
						onClick={this.handlePayment}/>
				</div>
				<form id='juspay-payv3-nb-payment-form' className="juspay_inline_form">
					<input type='hidden' className='order_id' value={this.props.orderId} />
					<input type='hidden' className='merchant_id' value='ec_demo' />
					<input type='hidden' className='payment_method_type' value='NB' />
					<input type='hidden' id='juspay-payv3-nb-payment-method' className='payment_method' value={this.props.selectedBank} />
					<button type="submit" hidden className="make_payment" id='juspay-payv3-submit'>Pay</button>
					<input type='hidden' className='redirect' value='false' />
				</form>
			</Dimmer.Dimmable>
		);
	}
}

export default connect(mapStateToProps, actions)(PayV3NetBankingPaymentView)