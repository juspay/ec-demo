import request from 'superagent'
import { createAction } from 'redux-actions'
import {
	UPDATE_ORDER_ID,
	UPDATE_AMOUNT,
	UPDATE_CUSTOMER_ID,
	UPDATE_CUSTOMER_PHONE,
	UPDATE_CUSTOMER_EMAIL,
	UPDATE_INVALID_AMOUNT,
	CREATE_ORDER,
	ORDER_CREATE_SUCCESSFUL,
	ORDER_CREATE_FAILED,
	SYNC_ORDER_DETAILS,
	SET_ACTIVE_TAB,
	UPDATE_CARD_NUMBER,
	UPDATE_CARD_EXPIRY_MONTH,
	UPDATE_CARD_EXPIRY_YEAR,
	UPDATE_CARD_SECURITY_CODE,
	UPDATE_CARD_TOKEN,
	UPDATE_CARD_BRAND,
	UPDATE_CARD_VALIDATION,
	UPDATE_SAVE_TO_LOCKER,
	GET_STORED_CARDS,
	UPDATE_STORED_CARDS,
	SHOW_LOADER,
	HIDE_LOADER,
	GET_PAYMENT_METHODS,
	SET_NB_OPTIONS,
	SET_WALLET_OPTIONS,
	SET_SUPPORTED_PAYMENT_METHODS,
	SET_SELECTED_BANK,
	SET_SELECTED_BANK_LABEL,
	SET_SELECTED_WALLET,
	SET_SELECTED_WALLET_LABEL,
	UPDATE_NB_VALIDATION,
	UPDATE_WALLET_VALIDATION
} from '../constants/ActionTypes'
import {
	SUCCESS,
	PENDING,
	FAILED,
	NEW,
	CREATING,
	INVALID
} from '../constants/Status'
import {
	GLOBAL_TIME_OUT,
	ORDER_CREATE_URL,
	LIST_CARDS_URL,
	PAYMENT_METHODS_URL,
	ORDER_STATUS_URL
} from '../constants/config'

const onUpdateOrderId = createAction(UPDATE_ORDER_ID, data => data)
const onUpdateAmount = createAction(UPDATE_AMOUNT, data => data)
const onUpdateCustomerId = createAction(UPDATE_CUSTOMER_ID, data => data)
const onUpdateCustomerPhone = createAction(UPDATE_CUSTOMER_PHONE, data => data)
const onUpdateCustomerEmail = createAction(UPDATE_CUSTOMER_EMAIL, data => data)
const onUpdateInvalidAmount = createAction(UPDATE_INVALID_AMOUNT)
const onCreateOrder = createAction(CREATE_ORDER)
const onOrderCreateSuccessful = createAction(ORDER_CREATE_SUCCESSFUL, data => data)
const onOrderCreateFailed = createAction(ORDER_CREATE_FAILED)
const onSyncOrderDetails = createAction(SYNC_ORDER_DETAILS, data => data)
const onSetActiveTab = createAction(SET_ACTIVE_TAB, data => data)
const onUpdateCardNumber = createAction(UPDATE_CARD_NUMBER, data => data)
const onUpdateCardToken = createAction(UPDATE_CARD_TOKEN, data => data)
const onUpdateCardExpiryMonth = createAction(UPDATE_CARD_EXPIRY_MONTH, data => data)
const onUpdateCardExpiryYear = createAction(UPDATE_CARD_EXPIRY_YEAR, data => data)
const onUpdateCardSecurityCode = createAction(UPDATE_CARD_SECURITY_CODE, data => data)
const onUpdateStoredCards = createAction(UPDATE_STORED_CARDS, data => data)
const onUpdateCardBrand = createAction(UPDATE_CARD_BRAND, data => data)
const onUpdateCardValidation = createAction(UPDATE_CARD_VALIDATION, data => data)
const onUpdateSaveToLocker = createAction(UPDATE_SAVE_TO_LOCKER, data => data)
const onShowLoader = createAction(SHOW_LOADER)
const onHideLoader = createAction(HIDE_LOADER)
const onSetNBOptions = createAction(SET_NB_OPTIONS, data => data)
const onSetWalletOptions = createAction(SET_WALLET_OPTIONS, data => data)
const onSetSupportedPaymentMethods = createAction(SET_SUPPORTED_PAYMENT_METHODS, data => data)
const onSetSelectedBank = createAction(SET_SELECTED_BANK, data => data)
const onSetSelectedWallet = createAction(SET_SELECTED_WALLET, data => data)
const onSetSelectedBankLabel = createAction(SET_SELECTED_BANK_LABEL, data => data)
const onSetSelectedWalletLabel = createAction(SET_SELECTED_WALLET_LABEL, data => data)
const onUpdateNBValidation = createAction(UPDATE_NB_VALIDATION, data => data)
const onUpdateWalletValidation = createAction(UPDATE_WALLET_VALIDATION, data => data)

const updateOrderId = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateOrderId(data))
	}
}

const updateAmount = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateAmount(data))
	}
}

const updateCustomerId = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCustomerId(data))
	}
}

const updateCustomerPhone = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCustomerPhone(data))
	}
}

const updateCustomerEmail = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCustomerEmail(data))
	}
}

const updateInvalidAmount = () => {
	return function(dispatch, getState) {
		dispatch(onUpdateInvalidAmount())
	}
}

const createOrder = (data) => {
	return function(dispatch, getState) {
		dispatch(onShowLoader())
		dispatch(onCreateOrder())
		request.post(ORDER_CREATE_URL)
				.timeout(GLOBAL_TIME_OUT)
				.send(data)
				.end((err, res) => {
					if(err || res.status >= 400) {
						dispatch(onOrderCreateFailed())
					} else {
						var response = res.body
						if(response && response.status != 'SUCCESS') {
							dispatch(onOrderCreateSuccessful(response))
							dispatch(onHideLoader())
						} else {
							dispatch(onOrderCreateFailed())
							dispatch(onHideLoader())
						}
					}
				})
	}
}

const getStoredCards = (data) => {
	return function(dispatch, getState) {
		dispatch(onShowLoader())
		if(data) {
			request.get(LIST_CARDS_URL.replace(':customerId', data))
				.timeout(GLOBAL_TIME_OUT)
				.end((err, res) => {
					if(err || res.status >= 400) {
						dispatch(onHideLoader())
					} else {
						var response = res.body
						dispatch(onUpdateStoredCards(response.cards))
						dispatch(onHideLoader())
					}
				})
		} else {
			dispatch(onUpdateStoredCards([]))
			dispatch(onHideLoader())
		}
	}
}

const syncOrderDetails = (orderId) => {
	return function(dispatch, getState) {
		dispatch(onShowLoader())
		dispatch(onUpdateOrderId(orderId))
		request.get(ORDER_STATUS_URL.replace(':orderId', orderId))
				.timeout(GLOBAL_TIME_OUT)
				.end((err, res) => {
					if(err || res.status >= 400) {
						dispatch(onHideLoader())
					} else {
						var response = res.body
						dispatch(onSyncOrderDetails(response))
						dispatch(onHideLoader())
					}
				})
	}
}

const getPaymentMethods = () => {
	return function(dispatch, getState) {
		request.get(PAYMENT_METHODS_URL)
			.timeout(GLOBAL_TIME_OUT)
			.end((err, res) => {
				if(err || res.status >= 400) {

				} else {
					var response = res.body
					var paymentMethods = response.payment_methods
					var nbOptions = []
					var walletOptions = []
					var supportedPaymentMethods = []
					if(paymentMethods) {
						paymentMethods.forEach((paymentMethod) => {
							if(typeof paymentMethod == 'string') {
								paymentMethod = JSON.parse(paymentMethod)
							}
							var option = {
								text : paymentMethod.description,
								value : paymentMethod.payment_method
							}
							if(paymentMethod.payment_method_type == 'NB') {
								nbOptions.push(option)
							} else if(paymentMethod.payment_method_type == 'WALLET') {
								walletOptions.push(option)
							}
							supportedPaymentMethods.push(paymentMethod.payment_method)
						})
					}
					dispatch(onSetNBOptions(nbOptions))
					dispatch(onSetWalletOptions(walletOptions))
					dispatch(onSetSupportedPaymentMethods(supportedPaymentMethods))
				}
			})
	}
}

const setActiveTab = (data) => {
	return function(dispatch, getState) {
		dispatch(onSetActiveTab(data))
	}
}

const updateCardNumber = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCardNumber(data))
	}
}

const updateCardToken = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCardToken(data))
	}
}

const updateCardExpiryMonth = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCardExpiryMonth(data))
	}
}

const updateCardExpiryYear = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCardExpiryYear(data))
	}
}

const updateCardSecurityCode = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCardSecurityCode(data))
	}
}

const updateSaveToLocker = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateSaveToLocker(data))
	}
}

const updateCardBrand = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCardBrand(data))
	}
}

const updateCardValidation = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateCardValidation(data))
	}
}

const showLoader = () => {
	return function(dispatch, getState) {
		dispatch(onShowLoader())
	}
}

const hideLoader = () => {
	return function(dispatch, getState) {
		dispatch(onHideLoader())
	}
}

const setSelectedBank = (data) => {
	return function(dispatch, getState) {
		dispatch(onSetSelectedBank(data))
	}
}

const setSelectedWallet = (data) => {
	return function(dispatch, getState) {
		dispatch(onSetSelectedWallet(data))
	}
}

const setSelectedBankLabel = (data) => {
	return function(dispatch, getState) {
		dispatch(onSetSelectedBankLabel(data))
	}
}

const setSelectedWalletLabel = (data) => {
	return function(dispatch, getState) {
		dispatch(onSetSelectedWalletLabel(data))
	}
}

const updateNBValidation = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateNBValidation(data))
	}
}

const updateWalletValidation = (data) => {
	return function(dispatch, getState) {
		dispatch(onUpdateWalletValidation(data))
	}
}

export const actions = {
	updateOrderId,
	updateAmount,
	updateCustomerId,
	updateCustomerPhone,
	updateCustomerEmail,
	updateInvalidAmount,
	createOrder,
	setActiveTab,
	getPaymentMethods,
	syncOrderDetails,
	updateCardNumber,
	updateCardToken,
	updateCardExpiryMonth,
	updateCardExpiryYear,
	updateCardSecurityCode,
	updateCardBrand,
	updateCardValidation,
	updateSaveToLocker,
	getStoredCards,
	showLoader,
	hideLoader,
	setSelectedBank,
	setSelectedWallet,
	setSelectedBankLabel,
	setSelectedWalletLabel,
	updateNBValidation,
	updateWalletValidation
}