# Express Checkout Demonstration Project #

This is an example project that you can setup easily in your localhost or Heroku. The project shows all the modes of checkout and user can transact end to end depending on the payment gateways that you have configured in your account. It is written in Python and uses our Python server SDK.   

## Demo Link :   [Click here](https://ecdemo.herokuapp.com)

### Project Setup : ###

* Install required python packages using ``` pip install -r requirements.txt``` 
* Install npm dependencies using ```npm install```

### How to run the project : ###

* Create a config file ```JuspayConfig.py``` in the server directory with the following contents : 

```
merchant_id = 'merchant_id'
environment = 'sandbox'
api_key = 'your_api_key'
return_url = 'https://localhost:5000/orders/:orderId/receipt'
```

* To build the project, run ```npm run build```
* To start the server, run ```npm run server```
* For webpack dev server, run ```npm run client```